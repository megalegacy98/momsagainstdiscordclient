#MomsAgainstDiscord Client
The MomsAgainstDiscord Client is a multiplatform client aimed to make taking screenshots, gifs, and uploading files, easier through the MomsAgainstDiscord API

#Download
- For Linux: Coming soon
- For Mac: Coming soon
- For Windows: Coming soon

If the binary doesn't work, you could also try building from source, which I have instructions for below.


#Build Instructions
On Linux:

- Install dependencies:
	- qt
	- qmake
	- libcurl
	- xcb (libxcb-keysyms1)
	- opengl libraries (libgl1-mesa-dev)
- Clone this repository
- Run build_script.sh
- The final build should be in your directory under "MomsAgainstDiscord"!

On Mac:

- Instructions coming soon

On Windows:

- Instructions coming soon
