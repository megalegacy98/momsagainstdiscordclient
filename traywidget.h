#ifndef TRAYWIDGET_H
#define TRAYWIDGET_H

#include <QSystemTrayIcon>
#include <QMenu>
#include <QCoreApplication>
#include <QApplication>
#include <QRect>
#include <QDesktopWidget>

#include <iostream>
#include <sstream>

#include <uglobalhotkeys.h>

#include "configwindow.h"
#include "screenshothandler.h"

class TrayWidget : public QSystemTrayIcon
{
    Q_OBJECT

public:
    TrayWidget();
    ~TrayWidget();
private:
    QMenu* menu;
    QAction* settings;
    QAction* exit;

    ConfigWindow* window;

    ScreenshotHandler* handler;

    UGlobalHotkeys* take_scrn;


private slots:
    void openWindow();
    void exitProgram();

    void takeScreenshot();
    void cancel();

    void newNotification();

};

#endif // TRAYWIDGET_H
