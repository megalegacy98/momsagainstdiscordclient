#!/bin/bash

set -e

OUTPUT="$(uname -s)"

if [ "${OUTPUT}" = "Linux" ]; then
    if [ -d "build" ]; then
        rm -rf build
    fi

    if [ -f "linuxdeployqt.AppImage" ]; then
        rm linuxdeployqt.AppImage
    fi

    if [ -f "MomsAgainstDiscord" ]; then
        rm MomsAgainstDiscord
    fi

    mkdir build
    cd build

    # Build cpr and put stuff in the main folder
    git clone https://github.com/Megalegacy98/cpr
    cd cpr
    git submodule update --init --recursive
    mkdir build
    cd build
    cmake ..
    make
    cp ./lib/* ../../
    cd ..
    cd ..

    # Build UGlobalHotkey and put stuff in the main folder
    git clone https://github.com/falceeffect/UGlobalHotkey
    cd UGlobalHotkey
    qmake
    make
    cp libUGlobalHotkey.so.1.0.0 ../
    cd ..
    mv libUGlobalHotkey.so.1.0.0 libUGlobalHotkey.so
    ln -s libUGlobalHotkey.so libUGlobalHotkey.so.1

    # Build libclipboard and put stuff in main folder
    git clone https://github.com/jtanx/libclipboard
    cd libclipboard
    mkdir build
    cd build
    cmake -DBUILD_SHARED_LIBS=ON ..
    make
    cp ./lib/*.so ../../
    cp ./include/* ../include
    cd ..
    cd ..

    # Copy stuff to /usr/lib temporarily so it gets recognized by linuxdeployqt
    sudo cp libUGlobalHotkey.so /usr/lib
    sudo cp libUGlobalHotkey.so.1 /usr/lib
    sudo cp libcpr.so /usr/lib
    sudo cp libclipboard.so /usr/lib
    rm libUGlobalHotkey.so.1
    rm libUGlobalHotkey.so
    rm libcpr.so
    rm libclipboard.so

    # Build the actual program and get rid of extra files...
    qmake -o Makefile ../MomsAgainstDiscordClient.pro -spec linux-g++ CONFIG+=debug CONFIG+=qml_debug
    make
    rm *.o
    rm *.h
    rm *.cpp
    rm Makefile
    rm -rf cpr
    rm -rf UGlobalHotkey
    rm -rf libclipboard
    mv MomsAgainstDiscordClient MomsAgainstDiscord

    # Copy assets (icon)
    mkdir assets
    cp ../icon.png ./assets
    cp ../icon.png .
    mv icon.png MomsAgainstDiscord.png

    # Download desktop file
    wget https://gist.githubusercontent.com/Megalegacy98/078584a53e01fc8022d6e2da34fd8ce1/raw/453bec4ffc5edb4ec4cdeda4b0ecd8c1b2c5ed8a/MomsAgainstDiscord.desktop

    # Download linuxdeployqt
    cd ..
    wget https://github.com/probonopd/linuxdeployqt/releases/download/continuous/linuxdeployqt-continuous-x86_64.AppImage
    mv linuxdeployqt-continuous-x86_64.AppImage linuxdeployqt.AppImage
    chmod a+x linuxdeployqt.AppImage

    # Run linuxdepolyqt
    ./linuxdeployqt.AppImage ./build/MomsAgainstDiscord.desktop -appimage

    # Clean up...
    cd build
    rm MomsAgainstDiscord

    # Rename to MomsAgainstDiscord
    cd ..
    mv MomsAgainstDiscord-x86_64.AppImage MomsAgainstDiscord

    # Finally, clean up the remaining parts
    rm linuxdeployqt.AppImage
    rm -rf build
    sudo rm /usr/lib/libUGlobalHotkey.so.1
    sudo rm /usr/lib/libUGlobalHotkey.so
    sudo rm /usr/lib/libcpr.so
    sudo rm /usr/lib/libclipboard.so

    echo "Done!"
    echo "The program has succesfully builded under the file MomsAgainstDiscord."
fi
