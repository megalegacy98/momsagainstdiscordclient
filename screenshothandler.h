#ifndef SCREENSHOTHANDLER_H
#define SCREENSHOTHANDLER_H

#include <QWidget>
#include <QMouseEvent>
#include <QApplication>
#include <QRect>
#include <QDesktopWidget>
#include <QGraphicsOpacityEffect>
#include <QPalette>
#include <QScreen>
#include <QGuiApplication>
#include <QClipboard>
#include <QTimer>
#include <QBuffer>
#include <QString>
#include <QSystemTrayIcon>
#include <iostream>
#include <sstream>

#include <cpr/cpr.h>

#include <libclipboard.h>

class ScreenshotHandler : public QWidget
{
    Q_OBJECT
public:

    explicit ScreenshotHandler(QWidget *parent = nullptr);
    ~ScreenshotHandler();

    void prepareScreenshot(std::string d, std::string k);
    void takeScreenshot();
    void stopSelection();

    void setKey(std::string key);
    void setDomain(std::string domain);

private:

    void mouseMoveEvent(QMouseEvent* e);
    void mousePressEvent(QMouseEvent* e);
    void mouseReleaseEvent(QMouseEvent* e);
    void keyPressEvent(QKeyEvent*);

    QWidget* selection;
    QRect screen_size;

    int x = 0;
    int y = 0;
    int w = 0;
    int h = 0;

    QRect select_rect;

    bool initiated = false;

    QScreen* the_screen;
    QPixmap the_image;

    std::stringstream url_builder;
    std::string key;
    std::string domain;

    std::string keyForAuth;
    std::string domainToUploadTo;

    #if __linux__
    clipboard_c *cb;
    #endif

signals:
    void screenshotTaken();

};

#endif // SCREENSHOTHANDLER_H
