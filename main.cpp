#include "traywidget.h"
#include <QApplication>
#include <QMessageBox>
#include <uglobalhotkeys.h>
#include <iostream>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    a.setQuitOnLastWindowClosed(false);

    TrayWidget w;
    w.show();

    return a.exec();
}
