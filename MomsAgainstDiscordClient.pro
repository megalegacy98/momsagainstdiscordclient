#-------------------------------------------------
#
# Project created by QtCreator 2017-08-05T17:38:06
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = MomsAgainstDiscordClient
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        main.cpp \
        configwindow.cpp \
    traywidget.cpp \
    screenshothandler.cpp

HEADERS += \
        configwindow.h \
    traywidget.h \
    screenshothandler.h

FORMS += \
        configwindow.ui

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/build/UGlobalHotkey -lUGlobalHotkey
else:unix:{
    LIBS += -lUGlobalHotkey
}

INCLUDEPATH += $$PWD/build/UGlobalHotkey
DEPENDPATH += $$PWD/build/UGlobalHotkey

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/build/cpr/lib/release/ -lcpr
else:unix: LIBS += -lcpr

INCLUDEPATH += $$PWD/build/cpr/include
DEPENDPATH += $$PWD/build/cpr/include

unix: {
    LIBS += -lclipboard
    INCLUDEPATH += $$PWD/build/libclipboard/include
    DEPENDPATH += $$PWD/build/libclipboard/include
}
