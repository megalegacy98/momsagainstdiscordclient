#ifndef CONFIGWINDOW_H
#define CONFIGWINDOW_H

#include <QWidget>
#include <QFile>
#include <QDir>
#include <QByteArray>
#include <QTextStream>
#include <QStandardPaths>

#include <uglobalhotkeys.h>

#include <iostream>
#include <sstream>

namespace Ui {
class ConfigWindow;
}

class ConfigWindow : public QWidget
{
    Q_OBJECT

public:
    explicit ConfigWindow(UGlobalHotkeys* g, QWidget *parent = 0);
    ~ConfigWindow();

    QString config_location;
    QFile* config;
    QByteArray data;

    std::string getKey();
    std::string getDomain();
    std::string getScrnHotkeys();
    std::string getGIFHotkeys();

private:
    Ui::ConfigWindow *ui;

    UGlobalHotkeys* ha;

private slots:
    void apply();
};

#endif // CONFIGWINDOW_H
