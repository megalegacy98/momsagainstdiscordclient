#include "configwindow.h"
#include "ui_configwindow.h"

ConfigWindow::ConfigWindow(UGlobalHotkeys* g, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ConfigWindow)
{
    ui->setupUi(this);

    std::stringstream the_builder;

    the_builder << QCoreApplication::applicationDirPath().toStdString() << "/assets/icon.png";

    this->setWindowTitle("Settings");
    this->setWindowIcon(QIcon(the_builder.str().c_str()));

    ha = g;

    std::string config_folder = QStandardPaths::writableLocation(QStandardPaths::ConfigLocation).toStdString();

    the_builder.clear();
    the_builder.str("");

    the_builder << config_folder << "/madclient";
    config_folder = the_builder.str();

    the_builder << "/settings.conf";
    config_location = QString(the_builder.str().c_str());

    the_builder.clear();
    the_builder.str("");

    if(!QDir(config_folder.c_str()).exists()) {
        QDir().mkdir(config_folder.c_str());
    }

    config = new QFile(config_location);
    config->open(QIODevice::ReadWrite);

    QTextStream in(config);
    while(!in.atEnd()) {
        QString line = in.readLine();
        if(line.startsWith("key")) {
            ui->key->setText(line.mid(4));
        } else if(line.startsWith("domain")) {
            ui->uploadUrl->setText(line.mid(7));
        } else if(line.startsWith("scrn_combo")) {
            ui->scrnshot_kb->setText(line.mid(11));
        } else if(line.startsWith("gif_combo")) {
            ui->gif_kb->setText(line.mid(10));
        }
    }

    connect(ui->pushButton, &QPushButton::clicked, this, &ConfigWindow::apply);

}

ConfigWindow::~ConfigWindow()
{
    config->close();
    delete config;
    delete ui;
}

std::string ConfigWindow::getKey()
{
    return ui->key->text().toStdString();
}

std::string ConfigWindow::getDomain()
{
    return ui->uploadUrl->text().toStdString();
}

std::string ConfigWindow::getScrnHotkeys()
{
    return ui->scrnshot_kb->text().toStdString();
}

std::string ConfigWindow::getGIFHotkeys()
{
    return ui->gif_kb->text().toStdString();
}

void ConfigWindow::apply()
{
    config->seek(0);
    config->write("key:");
    config->write(getKey().c_str());
    config->write("\ndomain:");
    config->write(getDomain().c_str());
    config->write("\nscrn_combo:");
    config->write(getScrnHotkeys().c_str());
    config->write("\ngif_combo:");
    config->write(getGIFHotkeys().c_str());
    config->resize(config->pos());

    ha->unregisterHotkey();
    ha->registerHotkey(getScrnHotkeys().c_str());
}
