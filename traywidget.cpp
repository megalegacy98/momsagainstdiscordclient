#include "traywidget.h"

TrayWidget::TrayWidget()
{
    std::stringstream the_builder;

    handler = new ScreenshotHandler();

    take_scrn = new UGlobalHotkeys();
    window = new ConfigWindow(take_scrn);

    take_scrn->registerHotkey(window->getScrnHotkeys().c_str());
    connect(take_scrn, &UGlobalHotkeys::activated, this, &TrayWidget::takeScreenshot);

    menu = new QMenu();
    settings = new QAction("Settings");
    exit = new QAction("Exit");

    connect(settings, &QAction::triggered, this, &TrayWidget::openWindow);
    connect(exit, &QAction::triggered, this, &TrayWidget::exitProgram);
    connect(handler, &ScreenshotHandler::screenshotTaken, this, &TrayWidget::newNotification);

    menu->addAction(settings);
    menu->addAction(exit);

    the_builder << QCoreApplication::applicationDirPath().toStdString() << "/assets/icon.png";

    this->setIcon(QIcon(the_builder.str().c_str()));
    this->setContextMenu(menu);

    the_builder.clear();
    the_builder.str("");
}

TrayWidget::~TrayWidget()
{
    delete exit;
    delete settings;
    delete menu;
    delete handler;
    delete take_scrn;
    delete window;
}

void TrayWidget::openWindow() {
    window->show();
}

void TrayWidget::exitProgram() {
    QApplication::quit();
}

void TrayWidget::takeScreenshot() {
    handler->prepareScreenshot(window->getDomain(), window->getKey());
}

void TrayWidget::cancel() {
    handler->stopSelection();
}

void TrayWidget::newNotification()
{
    this->showMessage(QString("Screenshot uploaded"), QString("The link to the screenshot is in your clipboard!"));
}
