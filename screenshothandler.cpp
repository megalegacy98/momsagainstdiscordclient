#include "screenshothandler.h"

ScreenshotHandler::ScreenshotHandler(QWidget *parent) : QWidget(parent)
{
    screen_size = QGuiApplication::primaryScreen()->geometry();
    std::cout << screen_size.width() << " " << screen_size.height();

    selection = new QWidget();

    QPalette pal = selection->palette();
    pal.setColor(QPalette::Background, Qt::black);

    selection->setWindowFlags(Qt::BypassWindowManagerHint | Qt::WindowStaysOnTopHint | Qt::FramelessWindowHint | Qt::NoDropShadowWindowHint | Qt::Tool);
    selection->setPalette(pal);
    selection->setWindowOpacity(0.2);
    selection->resize(0,0);

    std::cout << screen_size.width() << " " << screen_size.height() << std::endl;
    this->setWindowFlags(Qt::BypassWindowManagerHint | Qt::WindowStaysOnTopHint | Qt::FramelessWindowHint | Qt::NoDropShadowWindowHint | Qt::Tool);
    this->setAttribute(Qt::WA_TranslucentBackground, true);
    this->resize(screen_size.width(), screen_size.height());
    this->setMask(screen_size);

    this->setMouseTracking(true);

    #if __linux__
    cb = clipboard_new(NULL);
    #endif
}

ScreenshotHandler::~ScreenshotHandler()
{
    #if __linux__
    clipboard_free(cb);
    #endif
    delete selection;
}

void ScreenshotHandler::prepareScreenshot(std::string d, std::string k)
{
    domain = d;
    key = k;
    url_builder.clear();
    this->setCursor(Qt::CrossCursor);
    this->showFullScreen();
}

void ScreenshotHandler::takeScreenshot()
{
    the_screen = QGuiApplication::primaryScreen();
    the_image = the_screen->grabWindow(QApplication::desktop()->winId());
    the_image = the_image.copy(select_rect.x(), select_rect.y(), select_rect.width(), select_rect.height());
    the_image.save("tmp.png", "PNG");

    url_builder << "http://momsagainstdiscord.com/upload?key=" << key;

    auto res = cpr::Post(cpr::Url{url_builder.str()},
                         cpr::Multipart{
                             {
                                 "files[]", {cpr::File{"tmp.png"}}
                             }
    });

    url_builder.str("");

    std::cout << res.text << std::endl;

    QString image_part(res.text.c_str());
    image_part = image_part.replace("{\"success\":true,\"files\":{\"name\":\"tmp.png\",\"url\":\"", "");
    image_part = image_part.replace("\"}}", "");

    url_builder << "http://" << domain << image_part.toStdString();

    #if __linux__
    clipboard_set_text_ex(cb, url_builder.str().c_str(), -1, LCB_CLIPBOARD);
    #else
    QGuiApplication->clipboard->setText(url_builder.str().c_str());
    #endif

    url_builder.str("");
    QFile::remove("tmp.png");

    emit screenshotTaken();
}

void ScreenshotHandler::stopSelection()
{
    initiated = false;
    selection->close();
    this->close();
    x = 0;
    y = 0;
    w = 0;
    h = 0;
}

void ScreenshotHandler::mouseMoveEvent(QMouseEvent* e) {

    if(initiated) {
        int newW, newH, newX, newY;
        if((e->pos().x()) < x) {
            newX = e->pos().x();
            newW = (x - e->pos().x());
        } else {
            newX = x;
            newW = (e->pos().x() - x);
        }

        if((e->pos().y()) < y) {
            newY = e->pos().y();
            newH = (y - e->pos().y());
        } else {
            newY = y;
            newH = (e->pos().y() - y);
        }

        selection->move(newX, newY);
        selection->resize(newW, newH);
        select_rect.setX(newX);
        select_rect.setY(newY);
        select_rect.setWidth(newW);
        select_rect.setHeight(newH);
    }
}

void ScreenshotHandler::mousePressEvent(QMouseEvent* e)
{
    if(!initiated) {
        initiated = true;
        x = e->pos().x();
        y = e->pos().y();
        selection->move(x,y);
        selection->resize(0,0);
        selection->show();
    }
}

void ScreenshotHandler::mouseReleaseEvent(QMouseEvent*)
{
    if(initiated) {
        this->setCursor(Qt::ArrowCursor);
        this->stopSelection();
        QTimer::singleShot(200, this, &ScreenshotHandler::takeScreenshot);
    }
}

void ScreenshotHandler::keyPressEvent(QKeyEvent* e)
{
    if(e->key() == Qt::Key_Escape) {
        this->stopSelection();
    }
}

void ScreenshotHandler::setKey(std::string key)
{
    this->keyForAuth = key;
}

void ScreenshotHandler::setDomain(std::string domain)
{
    this->domainToUploadTo = domain;
}
